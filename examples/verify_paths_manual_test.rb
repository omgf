# -*- encoding: binary -*-
require "omgf/verify_paths"
require "logger"
require "uri"
require "pp"
vp = OMGF::VerifyPaths.new(Logger.new($stderr))
urls = %w(
  http://yhbt.net/
  http://yhbt.net/omgf.git
  http://yhbt.net/ozZZZZmgf.git
  http://yhbt.net/ozZZZZ
  http://yhbt.net/
  http://127.0.0.1:666/
)
uris = urls.map { |uri| URI(uri) }

pp vp.verify(uris, 1, 0.1)
uris = urls.map { |uri| URI(uri) }
sleep 1
pp vp.verify(uris, 1, 0.1)
