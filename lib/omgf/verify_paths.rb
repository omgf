# -*- encoding: binary -*-
# :stopdoc:
# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
# :startdoc:
require "thread"
require "omgf"
require "kcar"
require "kgio"

# This module makes HEAD requests with reusable HTTP connections to
# verify paths.  This is faster than having the mogilefsd tracker
# verifying paths, and the client could have broken routing to some
# the storage nodes the mogilefsd tracker/monitor can see.
class OMGF::VerifyPaths

  # private class
  class HeadSock < Kgio::Socket # :nodoc:
    VERSION = '1.0.0'

    attr_reader :uri
    attr_writer :retry_ok

    def self.start(uri)
      super(Socket.pack_sockaddr_in(uri.port, uri.host))
    end

    # returns true if the HTTP connection is reusable
    def http_reusable?
      rv = @kcar.keepalive?
      @kcar.reset
      @headers.clear
      @buf.clear
      @uri = nil
      rv ? setsockopt(:SOL_SOCKET, :SO_KEEPALIVE, 1) : close
      rv
    end

    def http_init(uri, retry_ok = true)
      @uri = uri
      unless defined?(@kcar)
        @kcar = Kcar::Parser.new
        @headers = {}
        @buf = ""
      end
      @retry_ok = retry_ok

      # prepare the HTTP request
      @req = "HEAD #{@uri.request_uri} HTTP/1.1\r\n" \
             "User-Agent: #{self.class}/#{VERSION}\r\n" \
             "Host: #{@uri.host}:#{@uri.port}\r\n" \
             "\r\n"
    end

    # returns an array result if successful
    # returns :wait_readable or :wait_writable if incomplete
    # returns a subclass of Exception on errors
    # returns nil on premature EOF (twice)
    def poll_iter(pollset)
      case rv = kgio_trywrite(@req)
      when nil, # done writing, start reading
           String # partial write
        @req = rv # continue looping
      when Symbol
        return pollset[self] = rv # busy
      end while @req

      case rv = kgio_tryread(666)
      when String
        if ary = @kcar.headers(@headers, @buf << rv)
          pollset.delete(self)
          return ary # success
        end
        # continue looping if incomplete
      when Symbol
        return pollset[self] = rv # busy
      when nil # EOF (premature)
        return maybe_retry(nil, pollset)
      end while true
    rescue => err
      maybe_retry(err, pollset)
    end

    # returns the err object if we've already retried, nil otherwise
    def maybe_retry(err, pollset)
      pollset.delete(self)
      return err unless @retry_ok

      # always start a fresh connection on socket errors
      sock = self.class.start(@uri)
      sock.http_init(@uri, false)
      pollset[sock] = :wait_writable
    end
  end

  def initialize(logger)
    @pool = Hash.new { |hash,host_port| hash[host_port] = [] }
    @logger = logger
    @finishq = Queue.new
    @finisher = nil
    @lock = Mutex.new
    @pid = $$
  end

  def error(msg)
    @logger.error(msg) if @logger
  end

  def iter_check(ok, sock, pollset)
    rv = sock.poll_iter(pollset)
    case rv
    when Symbol # in progress
    when Array
      code = rv[0].to_i
      if 200 == code
        ok << sock.uri
      elsif code >= 100 && code <= 999
        error("HEAD #{sock.uri} returned HTTP code: #{code}")
      else
        error("HEAD #{sock.uri} returned #{rv.inspect} (kcar bug?)")
      end
      sock_put(sock)
    when nil # premature EOF
      error("HEAD #{sock.uri} hit socket EOF")
    else
      # exception or some other error return value...
      if rv.respond_to?(:message)
        error("HEAD #{sock.uri} error: #{rv.message} (#{rv.class})")
      else
        error("HEAD #{sock.uri} error (#{rv.class}): #{rv.inspect}")
      end
    end
    rv
  end

  # this runs in a background thread to cleanup all the requests
  # that didn't finish quickly enough
  def finisher(timeout = 10000)
    begin
      pollset = @finishq.pop # park here when idle

      while ready = Kgio.poll(pollset.dup, timeout)
        ready.each_key do |sock|
          sock.retry_ok = false
          # try to return good sockets back to the pool
          iter_check([], sock, pollset)
        end

        # try to stuff the pollset as much as possible for further looping
        while more = (@finishq.pop(true) rescue nil)
          pollset.merge!(more)
        end
      end

      # connections timed out, kill them
      pollset.each_key { |sock| sock.close }
    rescue => err
      error("#{err.message} (#{err.class})")
    end while true
  end

  # reorders URIs based on response time
  # This is the main method of this class
  def verify(uris, count, timeout)
    tout = (timeout * 1000).to_i
    pollset = {}
    ok = []

    uris.each do |uri|
      sock = sock_get(uri) and iter_check(ok, sock, pollset)
    end

    while ok.size < count && tout > 0 && ! pollset.empty?
      t0 = Time.now
      ready = Kgio.poll(pollset.dup, tout) or break
      tout -= ((Time.now - t0) * 1000).to_i

      ready.each_key do |sock|
        iter_check(ok, sock, pollset)
      end
    end

    finish(pollset) unless pollset.empty?
    [ok, uris - ok] # good URLs first
  end

  # recover any unfinished URLs in pollset asynchronously in the background
  def finish(pollset) # :nodoc:
    @finishq.push(pollset)
    @lock.synchronize do
      unless @finisher && @finisher.alive?
        @finisher = Thread.new { finisher }
      end
    end
  end

  # returns a string key for the connection pool
  def key_for(uri)
    "#{uri.host}:#{uri.port}"
  end

  # initializes a cached connection for +uri+ or creates a new one
  def sock_get(uri)
    key = key_for(uri)

    # detect forks and prevent sharing of connected sockets across processes
    @lock.synchronize do
      if @pid != $$
        @pid = $$
        @pool.clear
      end
    end

    while sock = @lock.synchronize { @pool[key].pop }
      begin
        # check if sock is still alive and idle
        # :wait_readable is good here
        break if sock.kgio_tryread(1) == :wait_readable
      rescue
        # ignore socket errors, we'll just give them a new socket
        # socket should've been idle, but it was not (or EOFed on us)
        # give them a new one
      end
      sock.close
    end

    sock ||= HeadSock.start(uri)
    sock.http_init(uri)
    sock
  rescue
    # we'll return nil on any errors
  end

  # returns an idle socket to the pool
  def sock_put(sock)
    key = key_for(sock.uri)
    sock.http_reusable? and @lock.synchronize { @pool[key] << sock }
  rescue => err
    error("HTTP reuse check failed: #{err.message} (#{err.class})")
  end
end
