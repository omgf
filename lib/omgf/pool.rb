# -*- encoding: binary -*-
# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
require "thread"
require "omgf"

module OMGF::Pool
  # TODO: backport these improvements to MogileFS::Pool (but break compat?)
  def pool_init(mg_opts)
    @mg_opts = mg_opts
    @pool = []
    @lock = Mutex.new
  end

  def pool_use(domain)
    # Array#pop is faster than Queue#pop since Queue#pop shifts off
    # the array internally, and taking the last element off the end is faster.
    mg = @lock.synchronize { @pool.pop || MogileFS::MogileFS.new(@mg_opts) }
    mg.domain = domain
    rv = yield mg
    @lock.synchronize do
      if @pool.size < 3
        @pool << mg
        mg = nil # prevent shutdown
      end
    end
    rv
  ensure
    # shutdown if we didn't return to the pool
    if mg
      mg.backend.shutdown rescue nil
    end
  end

  def mg_list_keys(env, domain, prefix, after, limit, &block) # :nodoc:
    pool_use(domain) { |mg| mg.list_keys(prefix, after, limit, &block) }
  end

  def mg_get_uris(env, domain, key, opts) # :nodoc:
    pool_use(domain) { |mg| mg.get_uris(key, opts) }
  end

  def mg_size_and_uris(env, domain, key, opts) # :nodoc:
    pool_use(domain) do |mg|
      [ mg.size(key), mg.get_uris(key, opts) ]
    end
  end
end
