# -*- encoding: binary -*-
require 'omgf/hysterical_raisins'
require 'regurgitator'

module OMGF::Regurgitator
  include Regurgitator::ListKeys
  include Regurgitator::FileInfo

  def regurgitator_init(db)
    @db = db
    file_info_init
  end

  def check_domain!(domain)
    get_dmid(domain) or
      raise MogileFS::Backend::UnregDomainError, "#{domain} not found"
  end

  def _log_err(env, m, e)
    l = env["rack.logger"] or return
    l.error("retrying #{m} against tracker: #{e.message} (#{e.class})")
  end

  def mg_list_keys(env, domain, prefix, after, limit)
    check_domain!(domain)
    list_keys(domain, prefix: prefix, after: after, limit: limit) do |x|
      yield(x[:dkey], x[:length], x[:devcount])
    end
  rescue Sequel::Error => e
    _log_err(env, "mg_list_keys", e)
    super
  end

  def _uris!(info)
    info[:uris].values.flatten! || []
  end

  def mg_get_uris(env, domain, key, get_path_opts = {})
    check_domain!(domain)
    info = file_info({}, domain, key) or return super
    _uris!(info)
  rescue Sequel::Error => e
    _log_err(env, "mg_get_uris", e)
    super
  end

  def mg_size_and_uris(env, domain, key, get_path_opts = {})
    check_domain!(domain)
    info = file_info({}, domain, key) or return super
    [ info[:length], _uris!(info) ]
  rescue Sequel::Error => e
    _log_err(env, "mg_size_and_uris", e)
    super
  end
end
