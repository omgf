manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{omgf}
  s.version = (ENV['VERSION'] || '0.0.9').dup

  s.authors = ["OMGF hackers"]
  s.email = %q{bofh@yhbt.net}
  s.description = File.read('README').split("\n\n")[1]
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://yhbt.net/omgf/'
  s.summary = 'hysterical REST API for MogileFS using Rack'
  s.test_files = Dir['test/test_*.rb']
  s.add_dependency('rack', ['~> 1.3'])
  s.add_dependency('kgio', ['~> 2.7'])
  s.add_dependency('kcar', ['~> 0.3'])
  s.add_dependency('mogilefs-client', ['~> 3.1'])
  s.add_development_dependency('regurgitator')
  s.add_development_dependency('sqlite3')

  s.licenses = %w(AGPL-3.0+)
end
