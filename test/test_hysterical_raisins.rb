# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
require './test/integration'
require 'rack/mock'
require 'open-uri'
require 'omgf/hysterical_raisins'
require 'digest/md5'

class TestHystericalRaisins < Test::Unit::TestCase
  include TestMogileFSIntegration
  def setup
    setup_mogilefs
    @err = StringIO.new
    logger = Logger.new(@err)
    @opts = { "rack.logger" => logger }
    @app = OMGF::HystericalRaisins.new(:hosts => @hosts, :logger => logger)
    @req = Rack::MockRequest.new(@app)
    @admin.create_domain("testdom")
  end

  def test_all
    resp = @req.get("/")
    assert_equal 200, resp.status, "/ response code is 200"
    assert_equal "", resp.body, "/ returns empty body"

    # domain listing
    resp = @req.get("/testdom", @opts)
    assert_equal 200, resp.status, "domain listing response code is 200"
    assert_equal "", resp.body, "domain listing is empty"
    assert_equal("text/plain", resp.headers["Content-Type"],
                 "content-type is text/plain")

    json = { "HTTP_ACCEPT" => "application/json" }

    # json domain listing
    resp = @req.get("/testdom", @opts.merge(json))
    assert_equal 200, resp.status, "domain listing response code is 200"
    assert_equal "[]", resp.body, "domain listing is empty array"
    assert_equal("application/json", resp.headers["Content-Type"],
                 "content-type is application/json")

    # missing domain
    resp = @req.get("/non-existent", @opts)
    assert_equal 0, @err.string.size
    assert_equal 404, resp.status, "non-existent domain listing gives 404"

    # missing domain (json)
    resp = @req.get("/non-existent", @opts.merge(json))
    assert_equal 0, @err.string.size
    assert_equal 404, resp.status, "non-existent domain listing gives 404"

    # PUT to domain listing fails
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    resp = @req.put("/non-existent", opts)
    assert_equal 0, @err.string.size
    assert_equal 404, resp.status, "non-existent domain listing PUT gives 404"

    # DELETEs to bad domains fail
    resp = @req.delete("/non-existent", opts)
    assert_equal 0, @err.string.size
    assert_equal 404, resp.status, "non-existent domain DELETE gives 404"
    resp = @req.delete("/non-existent/key", opts)
    assert_equal 0, @err.string.size
    assert_equal 406, resp.status, "non-existent domain DELETE gives 406"

    # PUT to bad domain fails
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    resp = @req.put("/non-existent/key", opts)
    assert_equal 0, @err.string.size
    assert_equal 406, resp.status, "bad domain PUT w/key: #{resp.inspect}"
    assert_equal "Invalid domain: non-existent\n", resp.body

    # PUT with bad key fails
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    resp = @req.put("/testdom/key%20space", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 406, resp.status, "bad key"

    # PUT with long key fails
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    resp = @req.put("/testdom/key#{'a' * 128}", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 406, resp.status, "bad key"

    # PUT to good domain succeeds
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    resp = @req.put("/testdom/key", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 200, resp.status

    # GET succeeds with redirect
    resp = @req.get("/testdom/key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 302, resp.status
    assert_equal "HELLO", open(resp["Location"]).read

    # PUT to good domain and REMOTE_USER succeeds succeeds with 201
    opts = @opts.merge(:input => StringIO.new("HELLO"))
    opts["REMOTE_USER"] = "root"
    resp = @req.put("/testdom/user_key", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 201, resp.status

    # plain-text key listing succeeds
    resp = @req.get("/testdom", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 200, resp.status, resp.inspect
    assert_equal 2, resp.body.split(/\n/).size
    assert_equal("text/plain", resp.headers["Content-Type"],
                 "content-type is text/plain")
    keys = resp.body.split(/\n/)
    assert_match(/\Akey\|5|[12]\z/ ,keys[0])
    assert_match(/\Auser_key\|5|[12]\z/ ,keys[1])

    # json key listing succeeds
    resp = @req.get("/testdom", @opts.merge(json))
    assert_equal 0, @err.string.size, @err.string
    assert_equal 200, resp.status, resp.inspect
    keys = JSON.parse(resp.body)
    assert_equal 2, keys.size
    assert_equal("application/json", resp.headers["Content-Type"],
                 "content-type is application/json")
    assert_equal "key", keys[0][0]
    assert_equal 5, keys[0][1]
    assert_operator keys[0][2], :>=, 1
    assert_operator keys[0][2], :<=, 2
    assert_equal "user_key", keys[1][0]
    assert_equal 5, keys[1][1]
    assert_operator keys[1][2], :>=, 1
    assert_operator keys[1][2], :<=, 2

    # HEAD of json listing succeeds
    resp = @req.head("/testdom", @opts.merge(json))
    assert_equal("application/json", resp.headers["Content-Type"],
                 "content-type is application/json")
    assert_equal 200, resp.status
    assert_equal "", resp.body

    # HEAD of text listing succeeds
    resp = @req.head("/testdom", @opts)
    assert_equal("text/plain", resp.headers["Content-Type"],
                 "content-type is text/plain")
    assert_equal 200, resp.status
    assert_equal "", resp.body

    # DELETE succeeds
    resp = @req.delete("/testdom/user_key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 204, resp.status, resp.inspect

    # DELETE again fails
    resp = @req.delete("/testdom/user_key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 404, resp.status, resp.inspect

    # unauthed PUT fails to overwrite
    opts = @opts.merge(:input => StringIO.new("FAIL"))
    resp = @req.put("/testdom/key", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 403, resp.status, resp.inspect

    # GET returns unchanged file
    resp = @req.get("/testdom/key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 302, resp.status
    assert_equal "HELLO", open(resp["Location"]).read

    # PUT succeeds with overwrite
    opts = @opts.merge(:input => StringIO.new("BLAH"))
    opts["REMOTE_USER"] = "root"
    opts["HTTP_X_OMGF_FORCE"] = "true"
    resp = @req.put("/testdom/key", opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 204, resp.status, resp.inspect

    # GET succeeds with redirect
    resp = @req.get("/testdom/key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 302, resp.status
    assert_equal "BLAH", open(resp["Location"]).read

    # HEAD shows size and metadata
    resp = @req.head("/testdom/key", @opts)
    assert_equal 0, @err.string.size, @err.string
    assert_equal 200, resp.status, resp.inspect
    assert_equal "4", resp["Content-Length"]
    assert_equal "BLAH", open(resp["X-Url-0"]).read

    # wait for replication
    if ENV["EXPENSIVE"]
      50.times do
        resp = @req.head("/testdom/key", @opts)
        assert_equal 200, resp.status, resp.inspect
        resp["X-Url-1"] and break
        sleep 0.5
      end
      assert_kind_of String, resp["X-Url-1"], resp.inspect
      assert_equal "BLAH", open(resp["X-Url-1"]).read

      # Location should not be in X-Alt-Location, too
      resp = @req.get("/testdom/key", @opts)
      assert_equal 302, resp.status, resp.inspect
      assert_kind_of URI, URI(resp["Location"])
      assert_kind_of URI, URI(resp["X-Alt-Location-0"])
      assert_nil resp["X-Alt-Location-1"]
    end

    reproxy_test
  end

  def reproxy_test
    @app.instance_variable_set(:@reproxy_path, "/reproxy")
    opts = @opts.merge("HTTP_X_OMGF_REPROXY" => "1")
    resp = @req.get("/testdom/key", opts)
    assert_equal 0, @err.string.size, @err.string
    t = Time.parse(resp["X-Redirect-Last-Modified"])
    assert_equal "\"#{t.to_i}\"", resp["ETag"]
    assert_equal "application/octet-stream", resp["X-Redirect-Content-Type"]
    assert_equal "BLAH", open(resp["Location"]).read
    assert_equal "/reproxy", resp["X-Accel-Redirect"]
    assert_nil resp.original_headers["Content-Type"], resp.inspect
    assert_nil resp["Last-Modified"]

    # no point in redirecting HEAD requests
    resp = @req.head("/testdom/key", opts)
    assert_equal 0, @err.string.size, @err.string
    t = Time.parse(resp["Last-Modified"])
    assert_equal "\"#{t.to_i}\"", resp["ETag"]
    assert_nil resp["X-Accel-Redirect"]
    assert_equal "4", resp["Content-Length"]
    assert_equal "application/octet-stream",
                  resp.original_headers["Content-Type"], resp.inspect

    # explicit filename
    resp = @req.head("/testdom/key?inline=foo.txt", opts)
    assert_equal "inline; filename=foo.txt", resp["Content-Disposition"]
    resp = @req.head("/testdom/key?attachment=foo.txt", opts)
    assert_equal "attachment; filename=foo.txt", resp["Content-Disposition"]
    resp = @req.head("/testdom/key?filename=foo.txt", opts)
    assert_equal "attachment; filename=foo.txt", resp["Content-Disposition"]

    @app.instance_variable_set(:@reproxy_path, nil)
  end

  def test_checksums
    mogadm!("class", "add", "testdom", "md5", "--hashtype=MD5")
    100.times do
      @admin.get_domains["testdom"]["md5"] and break
      sleep 0.5
    end
    assert_equal "MD5", @admin.get_domains["testdom"]["md5"]["hashtype"]
    sio = StringIO.new("HELLO")
    md5 = [ Digest::MD5.digest(sio.string) ].pack('m0')
    opts = @opts.merge(input: sio, method: "PUT", "HTTP_CONTENT_MD5" => md5)
    resp = @req.put("/testdom/cc?class=md5", opts)
    assert_equal 200, resp.status
    @mg = MogileFS::MogileFS.new(hosts: @hosts, domain: "testdom")
    info = @mg.file_info("cc")
    assert_equal "md5", info["class"]
    assert_equal "MD5:#{Digest::MD5.hexdigest(sio.string)}", info["checksum"]
  end

  def teardown
    teardown_mogilefs
  end
end
