# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
require './test/integration'
require 'rack/mock'
require 'open-uri'
require 'omgf/hysterical_raisins'
require 'digest/md5'

class TestHystericalRaisinsCmogstored < Test::Unit::TestCase
  include TestMogileFSIntegration
  def setup
    setup_mogilefs(nil, "cmogstored")
    @err = StringIO.new
    logger = Logger.new(@err)
    @opts = { "rack.logger" => logger }
    @app = OMGF::HystericalRaisins.new(:hosts => @hosts, :logger => logger)
    @req = Rack::MockRequest.new(@app)
    @admin.create_domain("testdom")
  end

  def test_checksums_trailer
    mogadm!("class", "add", "testdom", "md5", "--hashtype=MD5")
    100.times do
      @admin.get_domains["testdom"]["md5"] and break
      sleep 0.5
    end
    assert_equal "MD5", @admin.get_domains["testdom"]["md5"]["hashtype"]

    sio = StringIO.new("HELLO")
    def sio.read(*_)
      rv = super
      if rv == nil
        md5 = [ Digest::MD5.digest(string) ].pack('m0')
        @rack_env["HTTP_CONTENT_MD5"] = md5
      end
      rv
    end

    opts = @opts.merge(input: sio, method: "PUT")
    opts["HTTP_TRAILER"] = "Content-MD5"
    env = @req.class.env_for("/testdom/cc?class=md5", opts)
    sio.instance_variable_set(:@rack_env, env)
    status, _, _ = @app.call(env)
    assert_equal 200, status, @err.string
    @mg = MogileFS::MogileFS.new(hosts: @hosts, domain: "testdom")
    info = @mg.file_info("cc")
    assert_equal "md5", info["class"]
    assert_equal "MD5:#{Digest::MD5.hexdigest(sio.string)}", info["checksum"]
  end

  def teardown
    teardown_mogilefs
  end
end if `which cmogstored 2>/dev/null` =~ /\bcmogstored\b/
