# Copyright (C) 2008-2013, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
require './test/integration'
require 'rack/mock'
require 'open-uri'
require 'omgf/hysterical_raisins'
require 'digest/md5'
require 'sequel' # regurgitator uses sequel
Sequel.extension :migration

class TestRegurgitator < Test::Unit::TestCase
  include TestMogileFSIntegration
  def setup
    setup_mogilefs
    @admin.create_domain("testdom")
    @err = StringIO.new
    @mirror = Tempfile.new("db.mirror")
    logger = Logger.new(@err)
    @opts = { "rack.logger" => logger }
    @orig_db = Sequel.connect("sqlite://#{@dbname.path}")
    @mirror_db = Sequel.connect("sqlite://#{@mirror.path}")
    @orig_db.extension(:schema_dumper)

    mig = @orig_db.dump_schema_migration(:indexes => false, :same_db => true)
    mig = eval(mig)
    mig.apply(@mirror_db, :up)
    omgf_opts = {
      :hosts => @hosts,
      :logger => logger,
      :db => @mirror_db,
    }
    @app = OMGF::HystericalRaisins.new(omgf_opts)
    @req = Rack::MockRequest.new(@app)
  end

  def get_recent
    s = TCPSocket.new(@test_host, @tracker_port)
    s.write("!recent\r\n")
    lines = []
    while line = s.gets
      break if line =~ /\A\.\r?\n/
      lines << line
    end
    lines
  ensure
    s.close if s
  end

  def test_listing
    # missing domain
    resp = @req.get("/non-existent", @opts)
    assert_equal 0, @err.string.size
    assert_equal 404, resp.status, "non-existent domain listing gives 404"
    lines = get_recent
    assert(/non-existent/ !~ lines.join, lines.inspect)

    # corrupt the mirror
    @mirror_db.disconnect
    @mirror.sync = true
    @mirror.truncate(0)
    @mirror.rewind
    @mirror.syswrite("\0" * (4096 * 1024))

    resp = @req.get("/non-existant", @opts)
    assert_match(/retrying mg_list_keys against tracker/, @err.string)
    assert_equal 404, resp.status, "non-existant domain listing gives 404"
    lines = get_recent
    assert_match(/non-existant/, lines.join, lines.inspect)
  end

  def teardown
    teardown_mogilefs
  end
end
