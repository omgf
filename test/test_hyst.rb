# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
require './test/integration'
require 'omgf/hysterical_raisins'
begin
  require 'unicorn'
rescue LoadError
end

class TestHystScript < Test::Unit::TestCase
  include TestMogileFSIntegration
  def setup
    setup_mogilefs
    @admin.create_domain("testdom")
    srv = TCPServer.new(@test_host, 0)
    @api_port = srv.addr[1]
    ENV["UNICORN_FD"] = srv.fileno.to_s
    api_addr = "#@test_host:#@api_port"
    @app = OMGF::HystericalRaisins.new(:hosts => @hosts)
    @out = Tempfile.new("out")
    @err = Tempfile.new("err")
    @unicorn_pid = fork do
      $stdin.reopen("/dev/null")
      $stdout.reopen(@out.path, "ab")
      $stderr.reopen(@err.path, "ab")
      # hopefully this Unicorn API doesn't change...
      Unicorn::HttpServer.new(@app, :listeners => [api_addr]).start.join
    end
    srv.close
    ENV["HYST_HOST"] = api_addr
    ENV["MOG_DOMAIN"] = 'testdom'
    @hyst = File.dirname(File.dirname(__FILE__)) + "/examples/hyst.bash"
    assert File.executable?(@hyst)
  end

  def test_hyst
    out = `#@hyst ls`
    assert_equal $?, 0
    assert_equal '', out

    # tee is a little tricky, unicorn is one of the few Rack servers that
    # handle chunked PUTs
    out = `echo HI | #@hyst tee foo`
    assert_equal 0, $?
    assert_equal "HI\n", out
    assert_equal "HI\n", `#@hyst cat foo`

    # ensure stat returns needed output
    lines = `#@hyst stat foo`.split(/\n/)

    key = lines.grep(/^Key: /)
    assert_equal 1, key.size, lines
    assert_equal "Key: foo", key[0]

    size = lines.grep(/^Size: /)
    assert_equal 1, size.size, lines
    assert_equal "Size: 3", size[0]

    urls = lines.grep(/^URL/)
    assert_match(/^URL-0: /, urls[0], urls.inspect)

    # /dev/null optimization
    out = `echo NULL | #@hyst tee bar >/dev/null`
    assert_equal 0, $?
    assert_equal "", out
    assert_equal "NULL\n", `#@hyst cat bar`

    # listings
    assert_equal "foo\n", `#@hyst ls fo`
    assert_equal "bar\n", `#@hyst ls b`
    assert_equal "", `#@hyst ls z`

    # remove a file once
    assert(system("#@hyst rm bar"))

    # really removed?
    err = Tempfile.new('curl_err')
    a = `#@hyst cat bar 2>#{err.path}`
    assert_equal 22, $?.exitstatus
    assert_equal '', a
    assert_match(/\s+404\b/, err.read)
    err.close!
  end

  def teardown
    ENV.delete("HYST_HOST")
    Process.kill :QUIT, @unicorn_pid
    _, status = Process.waitpid2(@unicorn_pid)
    assert status.success?, status.inspect
    teardown_mogilefs
  end
end if defined?(Unicorn)
