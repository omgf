# Copyright (C) 2008-2012, Eric Wong <normalperson@yhbt.net>
# License: AGPLv3 or later (https://www.gnu.org/licenses/agpl-3.0.txt)
$stdout.sync = $stderr.sync = true
Thread.abort_on_exception = true
if ENV["COVERAGE"]
  require "coverage"
  Coverage.start
  at_exit do
    # Dirty little text formatter.  I tried simplecov but the default
    # HTML+JS is unusable without a GUI (I hate GUIs :P) and it would've
    # taken me longer to search the Internets to find a plain-text
    # formatter I like...
    res = Coverage.result
    relevant = res.keys.grep(%r{/lib/omgf/\w+\.rb})
    relevant.each do |file|
      cov = res[file]
      puts "==> #{file} <=="
      File.readlines(file).each_with_index do |line, i|
        n = cov[i]
        if n == 0 # BAD
          print("  *** 0 #{line}")
        elsif n
          printf("% 7u %s", n, line)
        elsif line =~ /\S/ # probably a line with just "end" in it
          print("        #{line}")
        else # blank line
          print "\n" # don't output trailing whitespace on blank lines
        end
      end
    end
  end
end
require 'test/unit'
require 'net/http'
require 'uri'
require 'tempfile'
require 'mogilefs'
require 'stringio'
require 'logger'

module TestMogileFSIntegration
  def x(*cmd)
    out = Tempfile.new("out")
    err = Tempfile.new("err")
    puts cmd.join(' ') if $VERBOSE
    pid = fork do
      $stderr.reopen(err.path, "a")
      $stdout.reopen(out.path, "a")
      out.close
      err.close
      ObjectSpace.each_object(Tempfile) do |tmp|
        next if tmp.closed?
        ObjectSpace.undefine_finalizer(tmp)
        tmp.close_on_exec = true if tmp.respond_to?(:close_on_exec=)
      end
      exec(*cmd)
    end
    _, status = Process.waitpid2(pid)
    out.rewind
    err.rewind
    [ status, out, err ]
  end

  def x!(*cmd)
    status, out, err = x(*cmd)
    assert status.success?, "#{status.inspect} / #{out.read} / #{err.read}"
    [ status, out, err ]
  end

  def setup_mogilefs(plugins = nil, mogstored = "mogstored")
    @test_host = "127.0.0.1"
    setup_mogstored(mogstored)
    @tracker = TCPServer.new(@test_host, 0)
    @tracker_port = @tracker.addr[1]

    @dbname = Tempfile.new(["mogfresh", ".sqlite3"])
    @mogilefsd_conf = Tempfile.new(["mogilefsd", "conf"])
    @mogilefsd_pid = Tempfile.new(["mogilefsd", "pid"])

    cmd = %w(mogdbsetup --yes --type=SQLite --dbname) << @dbname.path
    x!(*cmd)

    @mogilefsd_conf.puts "db_dsn DBI:SQLite:#{@dbname.path}"
    @mogilefsd_conf.write <<EOF
conf_port #@tracker_port
listen #@test_host
pidfile #{@mogilefsd_pid.path}
replicate_jobs 1
fsck_jobs 1
query_jobs 1
mogstored_stream_port #@mogstored_mgmt_port
node_timeout 10
EOF
    @mogilefsd_conf.flush

    @trackers = @hosts = [ "#@test_host:#@tracker_port" ]
    @tracker.close
    x!("mogilefsd", "--daemon", "--config=#{@mogilefsd_conf.path}")
    wait_for_port @tracker_port
    @admin = MogileFS::Admin.new(:hosts => @hosts)
    500.times do
      break if File.size(@mogstored_pid.path) > 0
      sleep 0.05
    end

    args = { :ip => @test_host, :port => @mogstored_http_port }
    args[:status] = "alive"
    @admin.create_host("me", args)
    yield_for_monitor_update { @admin.get_hosts.empty? or break }

    mogadm!("device", "add", "me", "dev1")
    yield_for_monitor_update { @admin.get_devices.empty? or break }
    wait_for_usage_file "dev1"
    mogadm!("device", "add", "me", "dev2")
    wait_for_usage_file "dev2"
    yield_for_monitor_update { @admin.get_devices.size == 2 and break }
  end

  def mogadm(*args)
    x("mogadm", "--trackers=#{@trackers.join(',')}", *args)
  end

  def mogadm!(*args)
    status, out, err = mogadm(*args)
    assert status.success?, "#{status.inspect} / #{out.read} / #{err.read}"
    [ status, out, err ]
  end

  def yield_for_monitor_update
    50.times do
      yield
      sleep 0.1
    end
  end


  def wait_for_port(port)
    tries = 500
    begin
      TCPSocket.new(@test_host, port).close
      return
    rescue
      sleep 0.05
    end while (tries -= 1) > 0
    raise "#@test_host:#{port} never became ready"
  end

  def teardown_mogilefs
    if @mogstored_pid
      pid = File.read(@mogstored_pid.path).to_i
      Process.kill(:TERM, pid) if pid > 0
    end
    if @mogilefsd_pid
      s = TCPSocket.new(@test_host, @tracker_port)
      s.write "!shutdown\r\n"
      s.close
    end
    FileUtils.rmtree(@docroot)
  end

  def wait_for_usage_file(device)
    uri = URI("http://#@test_host:#@mogstored_http_port/#{device}/usage")
    res = nil
    100.times do
      res = Net::HTTP.get_response(uri)
      if Net::HTTPOK === res
        puts res.body if $DEBUG
        return
      end
      puts res.inspect if $DEBUG
      sleep 0.1
    end
    raise "#{uri} failed to appear: #{res.inspect}"
  end

  def setup_mogstored(mogstored = "mogstored")
    @docroot = Dir.mktmpdir(["mogfresh", "docroot"])
    Dir.mkdir("#@docroot/dev1")
    Dir.mkdir("#@docroot/dev2")
    @mogstored_mgmt = TCPServer.new(@test_host, 0)
    @mogstored_http = TCPServer.new(@test_host, 0)
    @mogstored_mgmt_port = @mogstored_mgmt.addr[1]
    @mogstored_http_port = @mogstored_http.addr[1]
    @mogstored_conf = Tempfile.new(["mogstored", "conf"])
    @mogstored_pid = Tempfile.new(["mogstored", "pid"])
    @mogstored_conf.write <<EOF
pidfile = #{@mogstored_pid.path}
maxconns = 500
httplisten = #@test_host:#@mogstored_http_port
mgmtlisten = #@test_host:#@mogstored_mgmt_port
docroot = #@docroot
EOF
    @mogstored_conf.flush
    @mogstored_mgmt.close
    @mogstored_http.close

    x!(mogstored, "--daemon", "--config=#{@mogstored_conf.path}")
    wait_for_port @mogstored_mgmt_port
    wait_for_port @mogstored_http_port
  end
end
